# Olympus

Welcome to Eryce FE Angular 4 (5) test.
Here is a brief info about what needs to be done.

Inside this folder you will find two .jpg files:
[ login-page.jpg, profile-page.jpg ]

#### First task
1. Convert those two .jpg files to working html templates following this rules:
	- you are free to choose your own font family, colors and rest of the assets (logos, images, etc.)
	- try to follow pages layout as much as possible
	- you can type styles using pure css or using some css preprocessors
	- you are free to use any of available FE framework ( Bootstrap, Foundation, MDL, etc.)
	- try to follow best HTML and CSS practices ( folder structure, element naming, css classes naming, etc.)
	- At the end compress everything to one zip file.


#### Second task 

2. Implementing Angular 4 (5) (after First task has been done)
	- you will need to install and implement new Angular 4 (5) library
	- convert html entities to angular 5 components ( try to find similar blocks and re-use components )
	[example: inside profile-page.jpg, middle column with blog posts, is there any repetition? 
		  Can post be a component with different data in it? You are free to mock all of the data inside]
 	- implement basic routing from login-page to profile-page [ you don’t need to implement any logic, just go from page to page using Angular router]
	
	After everything has been done, compress everything (do not include node_modules folder but keep package.json file)
	and send it back to us.

## Documentation

#### CSS Architecture
 Structurally it's developed by the influence of Harry Roberts ITCSS methodology
 with a few custom modifications if you're interested of reading more about this here is a link
 link for the article on Creative bloq -->  https://goo.gl/ghXiwX

#### Naming conventions
 1. The naming convection that we will be using for the code itself will be BEM
     If you aren't familiar with BEM you can catch up on this link --> https://goo.gl/YGB78d
 2. When writing classes that have a direct relationship
    with javascript please use the js-class-name convention

